====== Create Twilio channel ======
Twilio channel is the channel that the system will use [[http://www.twilio.com|Twilio account]] to receive and make a call. 
  - Click on drop down list and choose **"Twilio channel"**\\ {{:en:vbchannel:vb_going_to_create_twilio_channel.png?}}\\ 
  - Fill in the require information as bellow:
     * **Name**: is the name channel
     * **Account sid**: is provide by Twilio
     * **Auth token**: is provide by Twilio
  - Click button **Save** to finish\\ \\ {{:en:vbchannel:vb_creating_twilio_channel.png?nolink|}}